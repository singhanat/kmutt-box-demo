<?php

require_once('kmuttbox.php');

$config = array(
			'client_id' => 'YOUR_CLIENT_ID',
			'client_secret' => 'YOUR_CLIENT_SECRET'
		);

$kmuttbox = new Kmuttbox($config);

if($kmuttbox->getUser() != '')
{

	$param = array('path' => $_GET['path'], 'filename' => $_GET['filename']);
	$response = $kmuttbox->api('file', 'GET', $param);
		
	$file = json_decode($response, true);
	
	header('Location:' . $file['source']);
	
}

?>
