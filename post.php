<?php

require_once('kmuttbox.php');

$config = array(
			'client_id' => 'YOUR_CLIENT_ID',
			'client_secret' => 'YOUR_CLIENT_SECRET'
		);

$kmuttbox = new Kmuttbox($config);

if($kmuttbox->getUser() != '')
{
	move_uploaded_file($_FILES["file"]["tmp_name"], $_FILES["file"]["name"]);
	
	$param = array('path' => $_GET['path'], 'filename' => '@'.realpath($_FILES['file']['name']));
	$response = $kmuttbox->api('file', 'POST', $param);
	
	unlink($_FILES['file']['name']);
}

header('Location: index.php?path=' . $_GET['path']);

?>
