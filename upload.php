<?php

require_once('kmuttbox.php');

$config = array(
			'client_id' => 'YOUR_CLIENT_ID',
			'client_secret' => 'YOUR_CLIENT_SECRET'
		);

$kmuttbox = new Kmuttbox($config);

if($kmuttbox->getUser() != '')
{

	//// path
	if(isset($_GET['path']))
	{
		$path = $_GET['path'];
	}
	else
	{
		$path = '/';
	}

	//// display name
	
	$response = $kmuttbox->api('account_info', 'GET');
	
	$info = json_decode($response, true);
	
	$display_name = $info['first_name'] . ' ' . $info['last_name'] ;

}

?>

<?php if($kmuttbox->getUser() != ''):?>

<html>
<head>
<title>DEMO</title>
<link href="./css/index.css" type="text/css" rel="stylesheet" />
</head>
<body bgcolor="#D1D1D1" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (ui_Boom.png) -->
	<table id="Table_01" width="1440" height="719" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4" rowspan="2">
			<img src="images/DEMOindex_01.gif" width="281" height="128" alt=""></td>
		<td colspan="3">
			<img src="images/DEMOindex_02.gif" width="312" height="67" alt=""></td>
		<td rowspan="2">
			<img src="images/DEMOindex_03.gif" width="847" height="128" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" width="312" height="61" bgcolor="#bd4340">
			<table>
				<tr>
					<font face="Century Gothic" color="white" size="5">&nbsp;&nbsp;<?php print $display_name; ?> </font>
				</tr>
			</table>
		</td>	
	</tr>
	<tr>
		<td colspan="2  width="172" height="50" background="images/DEMOindex_08.gif">
		<font face="Century Gothic" size="5">&nbsp;&nbsp;Upload</font>
			<!--img src="images/DEMOindex_05.gif" width="172" height="50" alt=""--></td>
		<td width="39" height="50" background="images/DEMOindex_08.gif">
			<!--img src="images/DEMOindex_06.gif" width="39" height="50" alt=""--></td>
		<td colspan="2" width="100" height="50" background="images/DEMOindex_08.gif">
			<!--img src="images/DEMOindex_07.gif" width="100" height="50" alt=""--></td>
		<td width="211" height="50" background="images/DEMOindex_08.gif">	
		</td>
		<td colspan="2">
			<img src="images/DEMOindex_09.gif" width="918" height="50" alt="">
		</td>
	</tr>
	<tr>
		<td>
			<img src="images/DEMOindex_10.gif" width="81" height="40" alt=""></td>
		<td colspan="5">
		
			<font face="Century Gothic" color="#373737" size="3"><?php print $path; ?></font>
		</td>
		<td colspan="2">
			<img src="images/DEMOindex_12.gif" width="918" height="40" alt=""></td>
	</tr>
	<tr>
		<td colspan="8" width="1440" height="500" style="padding-left: 10px" valign="top">
		<form action="post.php?path=<?php print $path; ?>" method="post" enctype="multipart/form-data">
			<!--label for="file">&nbsp;&nbsp;Filename:</label-->
			&nbsp;<input type="file" name="file" id="file"><br>
			&nbsp;<input type="submit" name="submit" value="Submit">
		</form>
		</td>
	</tr>
	<tr>
		<td>
			<img src="images/spacer.gif" width="81" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="91" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="70" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="30" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="211" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="71" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="847" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>

<?php else:?>

<?php require_once('login.php'); ?>

<?php endif;?>
