<?php

require_once('kmuttbox.php');

$config = array(
			'client_id' => 'YOUR_CLIENT_ID',
			'client_secret' => 'YOUR_CLIENT_SECRET'
		);

$kmuttbox = new Kmuttbox($config);

if($kmuttbox->getUser() != '')
{
	$param = array('path' => $_GET['path']);
	$response = $kmuttbox->api('delete', 'POST', $param);
}

header('Location: index.php');

?>
