<?php

require_once('kmuttbox.php');

$config = array(
			'client_id' => 'YOUR_CLIENT_ID',
			'client_secret' => 'YOUR_CLIENT_SECRET'
		);

$kmuttbox = new Kmuttbox($config);

if($kmuttbox->getUser() != '')
{

	//// path
	if(isset($_GET['path']))
	{
		$path = $_GET['path'];
	}
	else
	{
		$path = '/';
	}

	//// display name
	
	$response = $kmuttbox->api('account_info', 'GET');
	
	$info = json_decode($response, true);
	
	$display_name = $info['first_name'] . ' ' . $info['last_name'] ;

	//// item list
	
	$param = array('path' => $path);
	$response = $kmuttbox->api('folder', 'GET', $param);
	
	$folder = json_decode($response, true);
	
}

?>

<?php if($kmuttbox->getUser() != ''):?>

<html>
<head>
<title>DEMO</title>
<link href="./css/index.css" type="text/css" rel="stylesheet" />
</head>
<body bgcolor="#D1D1D1" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (ui_Boom.png) -->
	<table id="Table_01" width="1440" height="719" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4" rowspan="2">
			<img src="images/DEMOindex_01.gif" width="281" height="128" alt=""></td>
		<td colspan="3">
			<img src="images/DEMOindex_02.gif" width="312" height="67" alt=""></td>
		<td rowspan="2">
			<img src="images/DEMOindex_03.gif" width="847" height="128" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" width="312" height="61" bgcolor="#bd4340">
			<table>
				<tr>
					<font face="Century Gothic" color="white" size="5">&nbsp;&nbsp;<?php print $display_name; ?> </font>
				</tr>
			</table>
		</td>	
	</tr>
	<tr>
		<td colspan="2">
			<a href="create_folder.php?path=<?php print $path; ?>"><img src="images/DEMOindex_05.gif" width="172" height="50" alt=""></a></td>
		<td>
			<img src="images/DEMOindex_06.gif" width="39" height="50" alt=""></td>
		<td colspan="2">
			<a href="upload.php?path=<?php print $path; ?>"><img src="images/DEMOindex_07.gif" width="100" height="50" alt=""></a></td>
		<td width="211" height="50" background="images/DEMOindex_08.gif">
			
			<!--img src="images/DEMOindex_08.gif" width="211" height="50" alt=""--></td>
		<td colspan="2">
			<img src="images/DEMOindex_09.gif" width="918" height="50" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/DEMOindex_10.gif" width="81" height="40" alt=""></td>
		<td colspan="5">
			<!--img src="images/DEMOindex_11.gif" width="441" height="40" alt=""-->
			<font face="Century Gothic" color="#373737" size="3"><?php print $path; ?></font>
		</td>
		<td colspan="2">
			<img src="images/DEMOindex_12.gif" width="918" height="40" alt=""></td>
	</tr>
	<tr>
		<td colspan="8" width="1440" height="500" style="padding-left: 10px" valign="top">
			<table style="width:800;" bgcolor="#fFFFFF" align="left">
				<tr bgcolor="#3a3a3a" align="center" style="width:120; height:40;">
			  		<td>
			  			<font face="Century Gothic" color="white" size="2"> Name </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="white" size="2"> Size </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="white" size="2"> Date </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="white" size="2"> Time </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="white" size="2"> Operation </font>
			  		</td>
				</tr>

	<?php 
	for($i=0; $i<$folder['found']; $i++) { 
		$link = 'download.php?path=' . $param['path'] . '&filename=' . $folder['itemlist'][$i]['name'];
		$delete_link = 'delete.php?path=' . $param['path'] . $folder['itemlist'][$i]['name'];
		$folder_link = 'index.php?path=' . $path . $folder['itemlist'][$i]['name'] . '/' ;
	?>				
				<tr style="width:120; height:40;">
			  		<td "bgcolor="#DADADA">
			  			&nbsp;&nbsp;
			  			
					<?php if($folder['itemlist'][$i]['type'] == 'file'): ?>	
						
						<img src="images/file.png" height="20" alt="">
			  			
					<?php else: ?>
					
						<img src="images/folder.png" height="20" alt="">
					
					<?php endif; ?>
						
						<a href="<?php if($folder['itemlist'][$i]['type'] == 'file') print $link; else print $folder_link; ?>" style="text-decoration:none">
						<font face="Century Gothic" color="gray" size="3">&nbsp;&nbsp; <?php print $folder['itemlist'][$i]['name']; ?> </font></a>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="gray" size="3">&nbsp;&nbsp;&nbsp;&nbsp; <?php if($folder['itemlist'][$i]['size'] > 0) print $folder['itemlist'][$i]['size']; ?> </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="gray" size="3">&nbsp;&nbsp;&nbsp;&nbsp; <?php print $folder['itemlist'][$i]['update_date']; ?> </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="gray" size="3">&nbsp;&nbsp;&nbsp;&nbsp; <?php print $folder['itemlist'][$i]['update_time']; ?> </font>
			  		</td>
			  		<td>
			  			<font face="Century Gothic" color="gray" size="3">&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php print $delete_link; ?>"><img src="./images/delete.png" /></a></font>
			  		</td>
				</tr>	
	<?php 
	} 
	?>	
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<img src="images/spacer.gif" width="81" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="91" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="70" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="30" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="211" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="71" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="847" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>

<?php else:?>

<?php require_once('login.php'); ?>

<?php endif;?>
